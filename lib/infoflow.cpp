//#include <ComplexityPass.hpp>
#include <Graphs/ICFG.h>
#include <Graphs/PAG.h>
#include "llvm/Support/Casting.h"
#include <boost/optional.hpp>
#include <functional>
#include <queue>
#include <unordered_map>
#include <utility>
#include <infoflow.hpp>
#include <gsl/pointers>
#include <iostream>
#include <unordered_set>

//using namespace boost::numeric;
//using namespace boost;
using namespace std;


/*
 *  flow sensitive -> get module -> produces all function
 *  From a specific function, can obtain basic blocks
 *  From basic blocks, can iterate through and get instructions (inst.getOpCode())
 *  Iterate through vector of values (sources) until it reaches the sink
 *  Iterate in reverse also (later)
 */



// Constructor
InfoFlowPass::InfoFlowPass(const FlowSensitive &mod,
                           std::vector<const llvm::Instruction *> sources,
                           const llvm::Instruction *sink) : FS(mod)
                           {

    this->sources = sources;
    this->sink = sink;

    cout << "\033[1;33m" << "BEGIN: INFOFLOWPASS CONSTRUCTOR"  << "\033[0m" << endl;
    try {
        cout << &mod << endl;
    }
    catch (...) {
        cout << "PRINT FAILURE - FlowSensitive" << endl;
    }
    try {
        cout << sources[0] << endl;
    }
    catch (...) {
        cout << "PRINT FAILURE - sources" << endl;
    }
    try {
        cout << sink << endl;
    }
    catch (...) {
        cout << "PRINT FAILURE - sink" << endl;
    }
    cout << "\033[1;33m" << "END: INFOFLOWPASS CONSTRUCTOR" << "\033[0m" << endl;



}

// Iterate over FS to assist in doing stuff
auto InfoFlowPass::iterate_over_functions() {
//    for (auto it : this->FS->getModule()) {
//        const Function *fun = it;
//            cout << "A WILD MEW APP- AND IT's gone" << endl;
//    }
    cout << "iterating over the things and shipping them to china" << endl;
}


// Returns the stuff
auto InfoFlowPass::get_branches() const -> const std::vector<const Instruction *> {
    cout << "GET_BRANCHES" << endl;

// ========================================


    // Obtain functions from FS for no reason whatsoever
    for (auto it : *FS.getModule()) {
        const Function *fun = it;
        cout << "FUNCTION:  ";
        cout << fun->getName().str() << endl;

        // If func
//        if (fun->getName().str() == "func") {
//            target = fun;
//            sources.insert(sources.begin(), fun);
//            for (const auto &bb : *fun) {
//                for (const auto &insn : bb) {
//                    if (insn.getOpcode() == Instruction::Ret) {
//                        sink = &insn;
//                    }
//                }
//            }
//        }

        // Obtain stuff from function
        for (const auto &bb : *fun) {
            for (const auto &insn : bb) {
                cout << "    VALUE:    ";
                cout << insn.getValueName() << endl;
                cout << "    OPCODE:    ";
                cout << insn.getOpcodeName() << endl;
            }
        }
    }

// ========================================

    PAG *pag = FS.getPAG();
    ICFG *icfg = pag->getICFG();

//    SVFG *svfg = this->FS.getSVFG();
    SVFGBuilder builder;
    SVFG *svfg = builder.buildFullSVFG(const_cast<FlowSensitive*>(&this->FS));

    // Find ICFG node (source)
    IntraBlockNode *source_node_ICFG = icfg->getIntraBlockNode(sources[0]);

    // Find ICFG node ID (source)
    NodeID source_node_ICFG_ID = source_node_ICFG->getId();

    // Get PAG Node ID (source)
    NodeID source_node_PAG_ID = pag->getValueNode(sources[0]);

    // Get PAG Node (source)
    const PAGNode *source_node_PAG = pag->getPAGNode(source_node_PAG_ID);

    // Get SVFG Node (source)
    const SVFGNode *source_node_SVFG = svfg->getDefSVFGNode(source_node_PAG);

    // Get SVFG Node ID (source)
    NodeID source_node_SVFG_ID = source_node_SVFG->getId();
    cout << "The  Node ID: " << source_node_SVFG_ID << endl;

    // Find ICFG node (sink)
    IntraBlockNode *sink_node_ICFG = icfg->getIntraBlockNode(sink);

    // Find ICFG node ID (sink)
    NodeID sink_node_ICFG_ID = sink_node_ICFG->getId();

    // Search from source to everything in SVFG graph, acquire branches
    // Then check if those branches exist in reverse, in ICFG graph

    std::vector<const SVFGNode*> stack;
    stack.push_back(source_node_SVFG);

//    auto source_out_edges = source_node_SVFG->getOutEdges();

    std::unordered_set<const SVFGNode*> visited_nodes;

    /*
     * SVFG graph search
     *
     * SOURCE NODE:         source_node_SVFG
     * STACK:               stack
     * NODES ALREADY SEEN:  visited_nodes
     *
     * Get Edges:           getOutEdges()
     * Get Node:            getDstNode()
     *
     */

    /*
     *
     * Procedure:
     * While stack is not empty:
     *   Check the vertex
     *      If visited
     *          go away
     *      If not visited
     *          mark as visited
     *          Add adjacent vertices to stack
     */

    // Search



    cout << "PREPARING DFS" << endl;

    while (!stack.empty()) {

        cout << "Stack is not empty; Processing node" << endl;

        // Remove node from stack
        const SVFGNode *the_node = stack.back();
        stack.pop_back();

        if (visited_nodes.find(the_node) != visited_nodes.end()) {
            continue;
        }

        // Mark node as visited
        visited_nodes.insert(the_node);

        // Obtain node edges
        auto the_node_edges = the_node->getOutEdges();

        // Acquire adjacent nodes
        for (const auto edge : the_node_edges) {

            cout << "Processing edge" << endl;

            // Get an adjacent node
            const auto end_node = edge->getDstNode();

            // Add note to stack
//            if (visited_nodes.find(the_node) == visited_nodes.end()) {
            stack.push_back(end_node);
//            }

        }

    }

    cout << "DFS COMPLETE" << endl;
    cout << visited_nodes.size() << endl;

    /*
     *
     * Nodes touched in SVFG        visited_nodes
     * ICFG sink node               sink_node_ICFG
     * ICFG source node             source_node_ICFG
     * ICFG sink nodeID             sink_node_ICFG_ID
     * The ICFG                     icfg
     */

    // Filter for only branch instructions from visited nodes


//    svfg->dump("PLZ");

//    std::unordered_set<const SVFGNode*> visited_branches;
    std::unordered_set<const Instruction*> tainted_branches_initial_set;

    for (const auto node : visited_nodes) {
        cout << "examining node" << endl;
        if (const auto node_thing = llvm::dyn_cast<CmpVFGNode>(node)) {
            cout << "Cmp node located" << endl;
            const Value * PAG_Valuge = node_thing->getRes()->getValue();

            for (const Use &use : PAG_Valuge->uses()) {
                cout << "word" << endl;

                use->print(llvm::outs());
                cout << endl;

                const User* VARIABLE = use.getUser();

                if (auto VARIABLE2 = llvm::dyn_cast<BranchInst>(VARIABLE)) {
                    cout << "We got him, boys" << endl;
                    tainted_branches_initial_set.insert(VARIABLE2);
                }

            }



        }
    }

    std::vector<const Instruction*> tainted_branches_final_set;
//    std::vector<const SVFGNode*> stack;

    std::vector<const ICFGNode*> ICFG_stack;
    std::unordered_set<const ICFGNode*> ICFG_visited_nodes;

    ICFG_stack.push_back(sink_node_ICFG);

    while (!ICFG_stack.empty()) {

        cout << "Stack not empty" << endl;

        // Remove node from stack
        const ICFGNode *current_node = ICFG_stack.back();
        ICFG_stack.pop_back();

        if (ICFG_visited_nodes.find(current_node) != ICFG_visited_nodes.end()) {
            cout << "Node" << endl;
            continue;
        }

        // Mark node as visited
        ICFG_visited_nodes.insert(current_node);

        // Perform magic
        if (auto current_node_cast = llvm::dyn_cast<IntraBlockNode>(current_node)) {
            const Instruction * instruction_var = current_node_cast->getInst();
            cout << "Instruction obtained" << endl;

            // Finalize the incantation
            if (tainted_branches_initial_set.find(instruction_var) != tainted_branches_initial_set.end()) {
                tainted_branches_final_set.push_back(instruction_var);
                cout << "We want this to happen, but I don't remember what this does" << endl;
            }
        }


        // Obtain node edges
        auto some_edges = current_node->getInEdges();

        // Acquire adjacent nodes
        for (const auto edge : some_edges) {

            cout << "Processing an edge (I think)" << endl;

            // Get an adjacent node
            const auto end_node = edge->getSrcNode();

            // Add note to stack
            if (ICFG_visited_nodes.find(end_node) == ICFG_visited_nodes.end()) {
                ICFG_stack.push_back(end_node);
            }

        }

    }




    // A branch is an instruction pointer


//    std::vector<const llvm::Instruction *> whatever;
    return tainted_branches_final_set;

}
