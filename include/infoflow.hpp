//#include <ComplexityPass.hpp>
#include <Graphs/ICFG.h>
#include <Graphs/PAG.h>
#include <boost/optional.hpp>
#include <functional>
#include <queue>
#include <unordered_map>
#include <utility>
#include <gsl/pointers>
#include <MemoryModel/MemModel.h>
#include <MemoryModel/PointerAnalysis.h>
#include <SVF-FE/LLVMUtil.h>
#include <WPA/FlowSensitive.h>
#include <boost/numeric/interval.hpp>
#include <boost/optional.hpp>
#include <boost/variant.hpp>
#include <functional>
#include <gsl/pointers>
#include <llvm/Support/Casting.h>
#include <unordered_map>

//using namespace boost::numeric;
using namespace boost;
using namespace std;

/*
 *
 * value flow graph forward from HV
 * ICFG backwards from sink
 *
 */


class InfoFlowPass {

// No private fields because we continue to disappoint Blerner (and Ian)
public:

    // Constructor
    InfoFlowPass(const FlowSensitive &mod,
                 std::vector<const llvm::Instruction *> sources,
                 const llvm::Instruction *sink);

    auto iterate_over_functions();

    // The function that does the thing with the stuff
    auto get_branches() const -> const std::vector<const llvm::Instruction *>;

    const FlowSensitive &FS;
    std::vector<const llvm::Instruction *> sources;
    const llvm::Instruction *sink;


};