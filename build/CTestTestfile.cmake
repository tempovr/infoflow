# CMake generated Testfile for 
# Source directory: /home/bro/netsec/Tempo/infoflow
# Build directory: /home/bro/netsec/Tempo/infoflow/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("external/SVF")
subdirs("lib")
subdirs("tests")
subdirs("external/Catch2")
