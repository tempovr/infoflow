long loop_until(long bound) {
    if (bound == 3) {
    	long b = 7;
	return b;
    }

    int a = 1;
    while (a < bound ) {
        a++;
    }
    return 1;
}

void expensive_function() {
    long a = loop_until(1000000);
}

void cheap_function() {
    int num_times = 10;
	long b = loop_until(num_times);
}

void func(int a) {
    if ( a==1 ) {
        expensive_function();
    } else {
        cheap_function();
    }
}
